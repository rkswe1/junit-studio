import org.junit.Test;
import static org.junit.Assert.*;

public class RomanNumeralTest {

    @Test
    public void testsSimpleRomanNumeralConversion() {
        String rtn = RomanNumeral.fromInt(4);
        assertEquals("IV", rtn);
    }

    @Test
    public void testsSimpleRomanNumeralConversion2() {
        String rtn = RomanNumeral.fromInt(7);
        assertEquals("VII", rtn);
    }

    @Test
    public void testsComplexRomanNumeralConversion() {
        String rtn = RomanNumeral.fromInt(999);
        assertEquals("CMXCIX", rtn);
    }

    @Test
    public void testsComplexRomanNumeralConversion2() {
        String rtn = RomanNumeral.fromInt(1982);
        assertEquals("MCMLXXXII", rtn);
    }
}
