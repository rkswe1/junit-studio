import org.junit.Test;
import static org.junit.Assert.*;


public class BalancedBracketsTest {

    //TODO: add tests here
    @Test
    public void testsSimpleBalancedBrackets() {
        boolean bRtn = BalancedBrackets.hasBalancedBrackets("[LaunchCode]");
        assertTrue(bRtn);
    }

    @Test
    public void testsMultipleBalancedBrackets() {
        boolean bRtn = BalancedBrackets.hasBalancedBrackets("Launch[Code]\", \"[]LaunchCode\", \"\", \"[]");
        assertTrue(bRtn);
    }

    @Test
    public void testsNestedBalancedBrackets() {
        boolean bRtn = BalancedBrackets.hasBalancedBrackets("Launch[Code[11]]\", \"[]LaunchCode\", \"\", \"[]");
        assertTrue(bRtn);
    }

    @Test
    public void testsMisAllignedNestedBalancedBrackets() {
        boolean bRtn = BalancedBrackets.hasBalancedBrackets("]Launch[Code]11[]\", \"[]LaunchCode\", \"\", \"[][");
        assertFalse(bRtn);
    }

    @Test
    public void testsUnBalancedBrackets() {
        boolean bRtn = BalancedBrackets.hasBalancedBrackets("[LaunchCode\", \"Launch]Code[\", \"[\", \"][");
        assertFalse(bRtn);
    }


}
