import org.junit.Test;
import static org.junit.Assert.*;

public class BinarySearchTest {

    @Test
    public void testsSimpleBinarySearch() {
        int [] searchStack = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int rtn = BinarySearch.binarySearch(searchStack, 7);
        assertEquals(7, rtn);
    }

    @Test
    public void testsBinarySearchTooHighNotFound() {
        int [] searchStack = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int rtn = BinarySearch.binarySearch(searchStack, 10);
        assertEquals(-1, rtn);
    }

    @Test
    public void testsBinarySearchTooLowNotFound() {
        int [] searchStack = {-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int rtn = BinarySearch.binarySearch(searchStack, -2);
        assertEquals(-1, rtn);
    }

}
